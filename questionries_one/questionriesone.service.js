﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const sendEmail = require('_helpers/send-email');
const db = require('_helpers/db');
var moment = require('moment');
var request = require('request');

module.exports = {

    section1SaveData,
    section2SaveData,
    section3SaveData,
    section4SaveData,
    section5SaveData,
    sendEmailOnCompletion,
    sendGift

};


async function section1SaveData(params) {
    const _id=params._id;
   
    const record=db.Questionries_one.findById(_id);
    if (record.hasOwnProperty('_id')) { 
   console.log('found');
   

    const rec =  db.Questionries_one.findByIdAndUpdate({_id},{

       
        section_1: {
    
            q1_id: params.q1_id,
            q1_ans: params.q1_ans,
            q2_id: params.q2_id,
            q2_ans: params.q2_ans
          
        
        
        }
        
        },function(err, result){
    
           if(err){  
               console.log('error1');
               console.log(err); 

            return 'Amir';
        } else { 
            console.log('error2');
            console.log(result);  }
            return 'inam';
       }
       );

  } else {
    console.log('Not found');
    const sec1={
        _id:_id,
         section_1: { q1_id: params.q1_id, q1_ans: params.q1_ans, q2_id: params.q2_id, q2_ans: params.q2_ans }
         }
        const Questionries_one = new db.Questionries_one(sec1);
   
   await  Questionries_one.save().then((user) => {
    // If everything goes as planed
    //use the retured user document for something
    return 'inam';
})
.catch((error) => {
    //When there are errors We handle them here
    console.log(error);
    return 'inam';

});

   
  
  }
   

    
}

//===================================================================
async function section2SaveData(params) {
    const _id=params._id;
  

  
    console.log(_id);
    const rec =  db.Questionries_one.findByIdAndUpdate({_id},{
     section_2: {
 
         q1_id: params.q1_id,
         q1_ans:params.q1_ans,
         q2_id: params.q2_id,
         q2_ans: params.q2_ans
 
 
     }
     
     },function(err, result){
 
        if(err){  console.log(err);  } else { console.log(result);  }
 
    });
 
 
     return 'inam';

}

async function section3SaveData(params) {
    const _id=params._id;
    const rec =  db.Questionries_one.findByIdAndUpdate({_id},{
     section_3: {
 
         q1_id: params.q1_id,
         q1_ans:params.q1_ans,
         q2_id: params.q2_id,
         q2_ans: params.q2_ans
  
     }
     
     },function(err, result){
 
        if(err){  console.log(err);  } else { console.log(result);  }
 
    });
 
 
     return 'inam';
}
async function section4SaveData(params) {
    const _id=params._id;
    const rec =  db.Questionries_one.findByIdAndUpdate({_id},{
     section_4: {
 
         q1_id: params.q1_id,
         q1_ans:params.q1_ans,
         q2_id: params.q2_id,
         q2_ans: params.q2_ans
 
     }
     
     },function(err, result){
 
        if(err){  console.log(err);  } else { console.log(result);  }
 
    });
 
 
     return 'inam';
}
async function section5SaveData(params) {
    const _id=params._id;
    const rec =  db.Questionries_one.findByIdAndUpdate({_id},{
     section_5: {
 
         q1_id: params.q1_id,
         q1_ans:params.q1_ans,
         q2_id: params.q2_id,
         q2_ans: params.q2_ans
 
     }
     
     },function(err, result){
 
        if(err){  console.log(err);  } else { console.log(result);  }
 
    });
 
 
     return 'inam'; 
}
async function addMonthFinishDate(param) {
    const account = await db.Account.findOne({ _id:param._id });
    account.oneMonth=1
    account.oneMonthFinishedDate=param.oneMonthFinishedDate;
    account.save();
} 


async function sendGift(param){
    const account = await db.Account.findOne({ _id:param._id });
    console.log(account);
    console.log('inam');
    console.log(account.oneMonthGift);
    if(account.oneMonthGift==0){

        request({
            method: 'POST',
            url: 'https://api-testbed.giftbit.com/papi/v1/campaign',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJTSEEyNTYifQ==.NlZBcXhna01KNlFOWGlrZ3hDUXJ2RmlqSm8yRzBKdHhSWnJCMUVZdXRETmZFaWlwRnRaYjlGeVdsZng4Z211Z2w1OEorVlFwRmRtT2tobjUzZmFKWklzTW03aG11eUtMSEdPY1BqVUhCSTZwWFFSZjhpcThwSGRqWkw0dUg1cWk=.8XlRptkeRSsUitGaHGxvMEokGr6zfmFddyChCRDFuoQ=',
              'Accept-Encoding': 'identity'
            },
            body: `{  \"gift_template\": \"CJHLMKCGSRLB\", \"contacts\": [{ \"firstname\": \"${param.firstName}\", \"lastname\": \"${param.lastName}\", \"email\": \"${param.email}\" }]}`

        }, function (error, response, body) {
            console.log('Status:', response.statusCode);
            console.log('Headers:', JSON.stringify(response.headers));
            console.log('Response:', body);
            account.oneMonth=1;
            account.oneMonthGift=1;
            account.save();
          });
 }


    
 }

//=========================================
async function sendEmailOnCompletion(param,origin) {
    console.log(param);
    console.log(origin);
    console.log(param.email);
   
    if (origin) {
      
        message = `<p>Dear </p> 
        <p>You will get a reminder email from Trial My App to complete the next set of questionnaires on trialmyapp.ca when its time. If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia Lokker at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013. This study has been reviewed by HIREB, project #8039.” </p> 
          <p>Thank you</p>
        <p>Trial My App Research Group</p>`;
    }

    await sendEmail({
        to: param.email,
        subject: 'Trial My App Confirmation Email - You have completed 1 month questionnaire',
        html: `
               ${message}`
    });
}

async function sendGift(param){
    const account = await db.Account.findOne({ _id:param._id });
    console.log(account);
    console.log('inam');
    console.log(account.zeroMonthGift);
    if(account.zeroMonthGift==0){

        request({
            method: 'POST',
            url: 'https://api-testbed.giftbit.com/papi/v1/campaign',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJTSEEyNTYifQ==.bDhtQW0yOTRuRm5UellWVHdDekZHems5OFpNUFJIeXlVMHF3OUlvTXUrbDFjODlNOXF4QnBDRVk0S1g0eHJVZDVjcktRbU9XZ1ZQa3dubkl2Q1pqNExQTXd5djhZbU9vRDBjaVpLNzZML3MrL1RYSndwSjdISmpRUWVRUmZqWTc=.ytcCA6b6/12yA2GSowaktiLbZNav6hR04zP3vsH/wpk=',
              'Accept-Encoding': 'identity'
            },
            body: "{  \"gift_template\": \"GDHHZFLSESAV\",\"subject\": \"Trial My App Confirmation Email - You have completed 0 month questionnaire\", \"message\": \"<p>You will get a reminder email from Trial My App to complete the next set of questionnaires on trialmyapp.ca when its time. If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia Lokker at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013. This study has been reviewed by HIREB, project #8039.” </p>  <p>Thank you</p><p>Trial My App Research Group</p>\",  \"contacts\": [    {      \"firstname\": \"Perry\",      \"lastname\": \"Johnson\",      \"email\": \"pjohnson@giftbit.com\"    },    {      \"firstname\": \"Rita\",      \"lastname\": \"Robson\",      \"email\": \"rrobson@giftbit.com\"    }  ]}"

        }, function (error, response, body) {
            console.log('Status:', response.statusCode);
            console.log('Headers:', JSON.stringify(response.headers));
            console.log('Response:', body);

            account.oneMonthGift=1;
            account.save();
          });
 }


    
 }
