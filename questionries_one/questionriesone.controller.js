﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const questiononeService = require('./questionriesone.service');

// routes

router.post('/section1SaveData', section1SaveDataSchema, section1SaveData);
router.post('/section2SaveData', section2SaveDataSchema, section2SaveData);
router.post('/section3SaveData', section3SaveDataSchema, section3SaveData);
router.post('/section4SaveData', section4SaveDataSchema, section4SaveData);
router.post('/section5SaveData', section5SaveDataSchema, section5SaveData);
router.post('/sendEmailOnCompletion',sendEmailOnCompletionSchema, sendEmailOnCompletion);
router.post('/sendGift', sendGift);





module.exports = router;



function section1SaveData(req, res, next) {
    console.log(req.body.user_id);
    questiononeService.section1SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section2SaveData(req, res, next) {
    console.log(req.body);
    questiononeService.section2SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}

function section3SaveData(req, res, next) {
    console.log(req.body);
    questiononeService.section3SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section4SaveData(req, res, next) {
    console.log(req.body);
    questiononeService.section4SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section5SaveData(req, res, next) {
    console.log(req.body);
    questiononeService.section5SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}




function sendEmailOnCompletion(req, res, next) {
   // console.log(req.body);
    questiononeService.sendEmailOnCompletion(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function sendGift(req, res, next) {
    // console.log(req.body);
    questiononeService.sendGift(req.body, req.get('origin'))
    .then((rec) =>  res.json(rec))
    .catch(next);
 }
//===================================================================================================
function section1SaveDataSchema(req, res, next) {
    console.log('Inam');
 
    
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required()
      

   
        
    });
    validateRequest(req, next, schema);
}
function section2SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required()

   
        
    });
    validateRequest(req, next, schema);
}
function section3SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required()

   
        
    });
    validateRequest(req, next, schema);
}

function section4SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required()
   
        
    });
    validateRequest(req, next, schema);
}


function section5SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required(),


   
        
    });
    validateRequest(req, next, schema);
}
function sendEmailOnCompletionSchema(req, res, next) {
  
    const schema = Joi.object({
        _id: Joi.string().required(),
        name: Joi.string().required(),
        email: Joi.string().required(),
                
    });
    validateRequest(req, next, schema);
}




