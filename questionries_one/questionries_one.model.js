const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
   
    _id: { type: String, trim: true},

    section_1:{ 
        q1_id: { type: String,trim: true},
        q1_ans: { type: String,trim: true},
        q2_id: { type: String, trim: true},
        q2_ans: { type: String,trim: true}
    },
    section_2:{ 
        q1_id: { type: String,trim: true},
        q1_ans: { type: String,trim: true},
        q2_id: { type: String, trim: true},
        q2_ans: { type: String,trim: true}
    },

    section_3:{ 
        q1_id: { type: String,trim: true},
        q1_ans: { type: String,trim: true},
        q2_id: { type: String, trim: true},
        q2_ans: { type: String,trim: true}
    },
 
     section_4:{ 
         q1_id: { type: String,trim: true},
         q1_ans: { type: String,trim: true},
         q2_id: { type: String, trim: true},
         q2_ans: { type: String,trim: true}
      },
      section_5:{ 
          q1_id: { type: String,trim: true},
          q1_ans: { type: String,trim: true},
          q2_id: { type: String, trim: true},
          q2_ans: { type: String,trim: true}
       }
      

});


module.exports = mongoose.model('Questionries_one', schema);
