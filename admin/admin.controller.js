﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const adminService = require('./admin.service');



// routes


router.post('/userList', userList);



module.exports = router;





function userList(req, res, next) {
    adminService.userList(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec)   )
        .catch(next);
}
