﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const profileService = require('./profile.service');



// routes

router.post('/saveProfile1', profileSaveSchema1, saveProfile1);
router.post('/saveProfile2', profileSaveSchema2, saveProfile2);
router.post('/check-profile', checkProfile);

router.post('/chk_questionries_six', chk_questionries_six);
router.post('/chk_questionries_three', chk_questionries_three);
router.post('/chk_questionries_zero', chk_questionries_zero);
router.post('/chk_questionries_one', chk_questionries_one);
router.post('/chk_screening_questionries', chk_screening_questionries);
router.post('/randomization', randomization);
router.post('/getGender', getGender);
router.post('/checkScreening', checkScreening);
router.post('/loadProfile', loadProfile);

module.exports = router;



function saveProfile1(req, res, next) {
    console.log(req.body);
    profileService.profileSave1(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}


function saveProfile2(req, res, next) {
    console.log(req.body);
    profileService.profileSave2(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}


function checkProfile(req, res, next) {
    
    profileService.checkProfile(req.body, req.get('origin'))
    .then((rec) =>  res.json(rec))
    .catch(next);
}

function profileSaveSchema1(req, res, next) {
    const schema = Joi.object({
        age: Joi.string().required(),
        weight: Joi.string().required(),
        height: Joi.string().required(),
        gender: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required(),
        q3_id: Joi.number().required(),
        q3_ans: Joi.string().required(),
        q4_id: Joi.number().required(),
        q4_ans: Joi.string().required(),
        weight_unit: Joi.string().required(),
        height_unit: Joi.string().required(),
        user_id: Joi.string().required()
   
        
    });
    validateRequest(req, next, schema);
}

function profileSaveSchema2(req, res, next) {
    const schema = Joi.object({
        user_id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required(),
        q3_id: Joi.number().required(),
        q3_ans: Joi.string().required(),
        q4_id: Joi.number().required(),
        q4_ans: Joi.string().required(),
        q5_id: Joi.number().required(),
        q5_ans: Joi.string().required(),
        q6_id: Joi.number().required(),
        q6_ans: Joi.string().required(),
        
   
        
    });
    validateRequest(req, next, schema);
}



function chk_questionries_six(req, res, next) {
    
    profileService.chk_questionries_six(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec))
        .catch(next);
}
function chk_questionries_three(req, res, next) {
    
    profileService.chk_questionries_three(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec))
        .catch(next);
}

function chk_questionries_one(req, res, next) {
    
    profileService.chk_questionries_one(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec))
        .catch(next);
}
function chk_questionries_zero(req, res, next) {
    
    profileService.chk_questionries_zero(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec))
        .catch(next);
}

function chk_screening_questionries(req, res, next) {
    
    profileService.chk_questionries_zero(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec))
        .catch(next);
}
function randomization(req, res, next) {
    
    profileService.randomization(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec))
        .catch(next);
}
function getGender(req, res, next) {
    
    profileService.getGender(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec)   )
        .catch(next);
}
function checkScreening(req, res, next) {
    
    profileService.checkScreening(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec)   )
        .catch(next);
}
function loadProfile(req, res, next) {
    profileService.loadProfile(req.body, req.get('origin'))
        .then((rec) =>  res.json(rec)   )
        .catch(next);
}
