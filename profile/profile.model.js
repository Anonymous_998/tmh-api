const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  
    _id:{ type: String, required: true,trim: true},
    
    section_1: {
    age: { type: String, required: true,trim: true},
    weight: { type: String, required: true,trim: true},
    weight_unit: { type: String, required: true,trim: true},
    height: { type: String, required: true,trim: true},
    height_unit: { type: String, required: true,trim: true},
    gender: { type: String, required: true,trim: true},
    q1_id: { type: Number, required: true,trim: true},
    q1_ans: { type: String, required: true,trim: true},
    q2_id: { type: Number, required: true,trim: true},
    q2_ans: { type: String, required: true,trim: true},
    q3_id: { type: Number, required: true,trim: true},
    q3_ans: { type: String, required: true,trim: true},
    q4_id: { type: Number, required: true,trim: true},
    q4_ans: { type: String, required: true,trim: true}
    },
    section_2: {
        q1_id: { type: Number, trim: true},
        q1_ans: { type: String, trim: true},
        q2_id: { type: Number, trim: true},
        q2_ans: { type: String, trim: true},
        q3_id: { type: Number, trim: true},
        q3_ans: { type: String, trim: true},
        q4_id: { type: Number, trim: true},
        q4_ans: { type: String, trim: true},
        q5_id: { type: Number, trim: true},
        q5_ans: { type: String, trim: true},
        q6_id: { type: Number, trim: true},
        q6_ans: { type: String, trim: true}
    }
});



module.exports = mongoose.model('Profile', schema);