﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const sendEmail = require('_helpers/send-email');
const db = require('_helpers/db');




module.exports = {

    profileSave1,
    profileSave2,
    checkProfile,
    chk_questionries_six,
    chk_questionries_three,
    chk_questionries_zero,
    chk_questionries_one,
    chk_screening_questionries,
    randomization,
    getGender,
    loadProfile


    
};


async function profileSave1(params) {
    // validate
    console.log(params);
   
    const sec1={
        _id:params.user_id,
        section_1: {
            age: params.age,
            weight: params.weight,
            height: params.height,
            height_unit: params.height_unit,
            weight_unit: params.weight_unit,
            gender: params.gender,
            q1_id: params.q1_id,
            q1_ans: params.q1_ans,
            q2_id: params.q2_id,
            q2_ans: params.q2_ans,
            q3_id: params.q3_id,
            q3_ans: params.q3_ans,
            q4_id: params.q4_id,
            q4_ans: params.q4_ans
            }


    }


    const Profile = new db.Profile(sec1);
   // save account
   await  Profile.save();
   // await sectionone.save();

    return 'inam';
}

async function profileSave2(params) {
    // validate
   // console.log(sectionone);

   const _id=params.user_id;
   const rec =  db.Profile.findByIdAndUpdate({_id},{
    section_2: {

        q1_id: params.q1_id,
        q1_ans:params.q1_ans,
        q2_id: params.q2_id,
        q2_ans: params.q2_ans,
        q3_id: params.q3_id,
        q3_ans: params.q3_ans,
        q4_id: params.q4_id,
        q4_ans: params.q4_ans,
        q5_id: params.q5_id,
        q5_ans: params.q5_ans,
        q6_id: params.q6_id,
        q6_ans: params.q6_ans

    }
    
    },function(err, result){

       if(err){  console.log(err);  } else { console.log(result);  }

   });


    return 'inam';
}

async function chk_questionries_six(params) {
    var res='';
    console.log(params);
   await db.Questionries_six.find({_id: params._id}, function (err, count){ 
        if(err){
            console.log(err);
         }
         if(count==''){
            res='/User/m4s6/Study_incentive_m4s6';
    
        } else if(count[0].section_1=='{}'){
           res='/User/m4s6/Study_incentive_m4s6';
        } else if(count[0].section_2=='{}'){
           res='/User/m4s6/Question_Three_m4s6';
 } else if(count[0].section_3=='{}'){
           res='/User/m4s6/Question_Four_m4s6';
        } else if(count[0].section_4=='{}'){
           res='/User/m4s6/Question_Five_m4s6';
        } else if(count[0].section_5=='{}'){
           res='/User/m4s6/Question_Six_m4s6';
        } else if(count[0].section_6=='{}'){
           res='/User/m4s6/Question_Seven_m4s6';
        } else if(count[0].section_7=='{}'){
           res='/User/m4s6/Question_Eight_m4s6';
        } else if(count[0].section_8=='{}'){
           res='/User/m4s6/Question_Nine_m4s6';
        } else if(count[0].section_9=='{}'){
           res='/User/m4s6/Question_Ten_m4s6';
        } else if(count[0].section_10=='{}'){
           res='/User/m4s6/Question_Elewen_m4s6';
        } else if(count[0].section_11=='{}'){
           res='/User/m4s6/Question_Twelve_m4s6';
        } else if(count[0].section_12=='{}'){
           res='/User/m4s6/Question_Thirteen_m4s6';
        } else if(count[0].section_13=='{}'){
           res='/User/m4s6/Question_Fourteen_m4s6';
        } else if(count[0].section_14=='{}'){
           res='/User/m4s6/Question_Fifteen_m4s6';
        } else {
            res='disable';
        }
        console.log(res);
    
    }); 
    var rec={ 
        page:res,
        msg:"Page found"
    };
    return rec;
 
}
async function chk_questionries_three(params) {
    var res='';
    console.log(params);
   await db.Questionries_three.find({_id: params._id}, function (err, count){ 
        if(err){
            console.log(err);
         }
         if(count==''){
            
            res='/User/m4s3/Question_One_m4s3';
            console.log(count);
     
        } else if(count[0].section_1=='{}'){
                res='/User/m4s3/Question_One_m4s3';
            } else if(count[0].section_2=='{}'){
                res='/User/m4s3/Question_Three_m4s3';
             
            } else if(count[0].section_3=='{}'){
                res='/User/m4s3/Question_Four_m4s3';
            } else if(count[0].section_4=='{}'){
                res='/User/m4s3/Question_Five_m4s3';
            } else if(count[0].section_5=='{}'){
                res='/User/m4s3/Question_Six_m4s3';
           
             } else {
                 res='disable';
             }
        console.log(res);
 
    }); 
    var rec={ 
        page:res,
        msg:"Page found"
    };
    return rec;
 
}
async function chk_questionries_one(params) {
    var res='';
    console.log(params);
   await db.Questionries_one.find({_id: params._id}, function (err, count){ 
        if(err){
            console.log(err);
         }

        
            if (count=='') {
                console.log('11111111111');
           res='/User/m4s1/Page_One';
         
   
            } else if(count[0].section_1=='{}'){
         res='/User/m4s1/Page_One';
             console.log('2');
              } else if(count[0].section_2=='{}'){
                res='/User/m4s1/Question_Four_m4s1';
                console.log('3');
            } else if(count[0].section_3=='{}'){
                res='/User/m4s1/Question_Five_m4s1';
                console.log('4');
            } else if(count[0].section_4=='{}'){
                res='/User/m4s1/Question_Six_m4s1';
            } else if(count[0].section_5=='{}'){
                res='/User/m4s1/Question_Seven_m4s1';
           
            }

    }); 
    var rec={ 
        page:res,
        msg:"Page found"
    };
    return rec;
 
}
async function chk_questionries_zero(params) {
    var res='';
    console.log(params);
   await db.Questionries_zero.find({_id: params._id}, function (err, count){ 
        if(err){
            console.log(err);
         }
         if(count==''){
            res='/User/m4s0/Question_Five';
    
        } else if(count[0].section_1=='{}'){
                res='/User/m4s0/Question_Five';
            } else if(count[0].section_2=='{}'){
                 res='/User/m4s0/Question_Six';
                } else if(count[0].section_3=='{}'){
                 res='/User/m4s0/Question_Seven';
                } else if(count[0].section_4=='{}'){
                 res='/User/m4s0/Question_Eight';
                } else if(count[0].section_5=='{}'){
                 res='/User/m4s0/Question_Nine';
                } else if(count[0].section_6=='{}'){
                 res='/User/m4s0/Question_Ten';
                } else if(count[0].section_7=='{}'){
                 res='/User/m4s0/Question_Elewen';
                } else if(count[0].section_8='{}'){
                 res='/User/m4s0/Question_Twelve';
                } else if(count[0].section_10=='{}'){
                 res='/User/m4s0/Question_Fourteen';
              
                      
             } else {
                 res='disable';
            
    }
    }); 
    var rec={ 
        page:res,
        msg:"Page found"
    };
    return rec;
 
}
async function chk_screening_questionries(params) {
    var res='';
    console.log(params);
   await db.Screening.find({_id: params._id}, function (err, count){ 
        if(err){
            console.log(err);
         }
         if (count=='') {
            console.log(count);
            res='/User/Eligibility_screener1';
               } else if(count[0].section_1=='{}'){
                res='/User/Eligibility_screener1';
                } else if(count[0].section_2=='{}'){
                 res='/User/Eligibility_screener2';
                } else if(count[0].section_3=='{}'){
                 res='/User/Eligibility_screener3';
                } else if(count[0].section_4=='{}'){
                 res='/User/Eligibility_screener4';
                } else if(count[0].section_5=='{}'){
                 res='/User/Eligibility_screener5';
                } else if(count[0].section_6=='{}'){
                 res='/User/Eligibility_screener6';
                } else if(count[0].section_7=='{}'){
                 res='/User/Eligibility_screener7';
                } else if(count[0].section_8=='{}'){
                 res='/User/Eligibility_screener8';
                } else if(count[0].section_9=='{}'){
                 res='/User/Eligibility_screener9';
                } else if(count[0].section_10=='{}'){
                 res='/User/Eligibility_screener10';
                } else if(count[0].section_11=='{}'){
                 res='/User/Eligibility_screener11';
                } else if(count[0].section_12=='{}'){
                 res='/User/Eligibility_screener12';
        
             } 
        console.log(res);
    
    }); 
    
    var rec={ 
        page:res,
        msg:"Page found"
    };
    return rec;
 
}
async function checkProfile(params) {
  
   var res1='';
   const profile= await db.Profile.find({_id: params._id});
   const screening= await db.Screening.find({_id: params._id});
   const questionries_zero= await db.Questionries_zero.find({_id: params._id});
console.log(profile);



   if (profile=='' || profile=='[]') {
    res1='/User/user_profile';
   
} else if(profile[0].section_2=='{}') {

    if(profile[0].section_1.q1_ans=='No'){  var a=1;  } else { var a=0; 
    }


console.log('user_profile_next/'+a);
    res1='/User/user_profile_next/'+a;

    
     } else   if (screening=='') {
            console.log(screening);
            res1='/User/Eligibility_screener1';
               } else if(screening[0].section_1=='{}'){
                res1='/User/Eligibility_screener1';
                } else if(screening[0].section_2=='{}'){
                    res1='/User/Eligibility_screener2';
              //  } else if(screening[0].section_3=='{}'){
             //       res1='/User/Eligibility_screener3';
                } else if(screening[0].section_4=='{}'){
                    res1='/User/Eligibility_screener4';
                } else if(screening[0].section_5=='{}'){
                    res1='/User/Eligibility_screener5';
                } else if(screening[0].section_6=='{}'){
                    res1='/User/Eligibility_screener6';


                    if(profile[0].section_1.gender=='Female'){

                } else if(screening[0].section_7=='{}'){
                    
                    res1='/User/Eligibility_screener7';

                }

                } else if(screening[0].section_8=='{}'){
                    res1='/User/Eligibility_screener8';
                } else if(screening[0].section_12=='{}'){
                    res1='/User/Eligibility_screener12';
                 } else if(screening[0].section_9=='{}'){
                    res1='/User/Eligibility_screener9';
                } else if(screening[0].section_10=='{}'){
                    res1='/User/Eligibility_screener10';
                } else if(screening[0].section_11=='{}'){
                    res1='/User/Eligibility_screener11';
                } else if(screening[0].section_13=='{}'){
                    res1='/User/m3/Conscent';
                } else if(screening[0].section_14=='{}'){
                    res1='/User/m3/Consent_confirm';

  } else if (questionries_zero=='' || questionries_zero=='[]' || questionries_zero[0].section_1=='[]') {
                    res1='/User/m4s0/Question_Five';
             
                } else if(questionries_zero[0].section_2=='{}'){

                    res1='/User/m4s0/Question_Six';
                } else if(questionries_zero[0].section_3=='{}'){

                    res1='/User/m4s0/Question_Seven';

                } else if(questionries_zero[0].section_4=='{}'){
                    res1='/User/m4s0/Question_Eight';
                } else if(questionries_zero[0].section_5=='{}'){
                     res1='/User/m4s0/Question_Nine';
                    } else if(questionries_zero[0].section_6=='{}'){
                        res1='/User/m4s0/Question_Ten';
                        } else if(questionries_zero[0].section_7=='{}'){
                        res1='/User/m4s0/Question_Elewen';
                    } else if(questionries_zero[0].section_8=='{}'){
                        res1='/User/m4s0/Question_Twelve';
                    }else if(questionries_zero[0].section_10=='{}'){
                        res1='/User/m4s0/Question_Fourteen';
                    } else {
                        res1='/User/Dashboard';
}

var rec={ 
    page:res1,
    msg:"Page found"
};
return rec;
}

async function randomization(params) {
      
  await db.randomization.findById({_id:params._id}, function(err, result) { 
  if(err){
            console.log(err);
            console.log('err');
         } 
         if (result) {
            console.log(result);
            console.log(randomization);
         }
     });
       return {
        Randomization
 } }  

    async  function getGender(params) {
      var gender;
      await  db.Profile.findById({_id:params._id}, function(err, result) { 
  
          if(err){
              console.log(err);
              console.log('err');
           } 
           if (result) {
              console.log(result);
              console.log(result.section_1.gender);
              gender=result.section_1.gender;
            
             
           }
       });
       rec={
        msg:'Done',
        rec:gender
    }
  return rec;

      }  

      async function loadProfile(params) {
        console.log(params._id);
        var res;
        var status;
        var checkFill;
        await db.Profile.findById({_id:params._id}, function(err, result) { 
        if(err){
                  console.log(err);
                  console.log('err');
               } 
               if (result) {
                  console.log(result);
                  if(result.section_1 !='{}'){
                  if(result.section_2 !='{}'){
                    checkFill='disabled';
                  }
                }
                  status='found';
                  res=result.section_1;
                } else {
                    status='not_found';
               }
           });
           rec={
            status:status,
            checkFill:checkFill,
            rec:res
        }
      return rec;
    } 