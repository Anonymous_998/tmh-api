const config = require('config.json');
const mongoose = require('mongoose');
const connectionOptions = { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false };
mongoose.connect(process.env.MONGODB_URI || config.connectionString, connectionOptions).then(() => {
    console.log('Connect to MongoDB');
});
mongoose.Promise = global.Promise;

module.exports = {
    Account: require('accounts/account.model'),
    Questionries_zero: require('questionries_zero/questionries_zero.model'),
    Questionries_one: require('questionries_one/questionries_one.model'),
    Questionries_three: require('questionries_three/questionries_three.model'),
    Questionries_six: require('questionries_six/questionries_six.model'),
    Randomization: require('randomization/randomization.model'),
    Profile: require('profile/profile.model'),
    Screening: require('screening_questionries/screening_questionries.model'),
    RefreshToken: require('accounts/refresh-token.model'),
   
    isValidId
};

function isValidId(id) {
    return mongoose.Types.ObjectId.isValid(id);
}