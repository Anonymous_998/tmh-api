﻿require('dotenv').config();
require('rootpath')();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const errorHandler = require('_middleware/error-handler');
const cron = require('node-cron');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
const accountService = require('accounts/account.service');
// allow cors requests from any origin and with credentials
//app.use(cors({ origin: (origin, callback) => callback(null, true), credentials: true }));
app.use(cors())
// api routes
app.use('/test', require('./test/test'));
app.use('/accounts', require('./accounts/accounts.controller'));
app.use('/questionrieszero', require('./questionries_zero/questionrieszero.controller'));
app.use('/questionriesone', require('./questionries_one/questionriesone.controller'));
app.use('/questionriesthree', require('./questionries_three/questionriesthree.controller'));
app.use('/questionriessix', require('./questionries_six/questionriessix.controller'));
app.use('/profile', require('./profile/profile.controller'));
app.use('/Screening', require('./screening_questionries/screening_questionries.controller'));
app.use('/randomization', require('./randomization/randomization.controller'));
app.use('/admin', require('./admin/admin.controller'));
// swagger docs route
app.use('/api-docs', require('_helpers/swagger'));

// global error handler
app.use(errorHandler);
// Schedule tasks to be run on the server.
cron.schedule('* * * * *', function() { 
    // console.log('Node Cron Schedular is working on every Minute');
    accountService.cronOne();
    accountService.cronThree();
    accountService.cronSix();
  });
// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4000;
app.listen(port, () => {
    console.log('Server listening on port ' + port);
});
