
const express = require('express');
const app = express.Router();
const mongoose = require('mongoose');
const Account = require('../accounts/account.model');
const Profile = require('../profile/profile.model');
const cors = require('cors')
const Questionries_zero = require('../questionries_zero/questionries_zero.model');
const request = require('request');
const { API_KEY, TEMPLATE_ID, GIFTBIT_URL } = process.env;

app.use(cors());
app.use(express.json());

app.post('/', (req, res) => {
    // console.log( req.body );
    if ( req.body.zeroMonthFinishedDate != undefined ) {
        Account.findOneAndUpdate({ _id:req.body._id }, {
            $set: {
                zeroMonthFinishedDate:req.body.zeroMonthFinishedDate
            }
        }, (err, result) => {
            setTimeout(() => {
                res.status(200).send(result);
            },3000);
            // console.log(result.zeroMonthFinishedDate,' ',result._id);
        });
    } else if ( req.body.oneMonthFinishedDate != undefined ) {
        console.log('One Months');
        Account.findOneAndUpdate({ _id:req.body._id }, {
            $set: {
                oneMonthFinishedDate:req.body.oneMonthFinishedDate
            }
        }, (err, result) => {
            setTimeout(() => {
                res.status(200).send(result);
            },3000);
        });
    } else if ( req.body.threeMonthFinishedDate != undefined ) {
        console.log('Three Months');
        Account.findOneAndUpdate({ _id:req.body._id }, {
            $set: {
                threeMonthFinishedDate:req.body.threeMonthFinishedDate
            }
        }, (err, result) => {
            setTimeout(() => {
                res.status(200).send(result);
            },3000);
        });
    } else if ( req.body.sixMonthFinishedDate != undefined ) {
        console.log('Six Months');
        Account.findOneAndUpdate({ _id:req.body._id }, {
            $set: {
                sixMonthFinishedDate:req.body.sixMonthFinishedDate
            }
        }, (err, result) => {
            setTimeout(() => {
                res.status(200).send(result);
            },3000);
        });
    }
});

app.post('/getCode', (req, res) => {
    console.log(req.body)
    Account.findById({ _id:req.body._id }, (err, doc) => {
        if(doc !== "" || doc !== null || doc !== undefined) {
            res.status(200).send(doc.randomization_code);
        } else {
            res.status(200).send("No Code Found");
        }
    });
});

app.post('/getStatus', (req, res) => {
    Account.findById({ _id:req.body._id }, (err, result) => {
        res.send(result);
    });
});

app.post('/sendGift', (req, res) => {
    // console.log(req.body);
    let firstName = req.body.name.split(' ');
    Account.findById({ _id:req.body._id }, (err, doc) => {
        if( doc.zeroMonthGift == 0 ) {
            request({
                method: 'POST',
                url: ''+GIFTBIT_URL+'', 
                headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+API_KEY+'',  
                    'Accept-Encoding': 'identity'
                },
                body: '{  "gift_template": "'+TEMPLATE_ID+'",  "contacts": [    {      "firstname": "'+firstName[0]+'",      "lastname": "'+firstName[1]+'",      "email": "'+req.body.email+'"    } ] }'
            }, (error, response, body) => {
                console.log(response);
            });
            Account.findOneAndUpdate({ _id:req.body._id }, {
                $set:{
                    "zeroMonth":1,
                    "zeroMonthGift":1,
                    "screeningQuestions":1
                }
            }, (err, doc) => {
                res.send(doc);
            });
        } else {
            res.send("Gift Already Sent");
        }
    });
});

app.post('/getDate', (req, res) => {
    console.log(req.body);
    Account.findOneAndUpdate({ _id:req.body._id }, {
        $set:{
            zeroMonth:1
        }
    }, (err, doc) => {
        console.log(doc);
        Account.findOne({ _id:req.body._id }, (err, result) => {
            res.status(200).send(result);
        });
    });
});

app.post('/getZeroFinishDate', (req, res) => {
    Account.findOne({ _id:req.body._id }, (err, data) => {
        if(data !== "" || data !== null || data!== undefined) {
            let dates = { "zeroMonth":"" };
            if( data.zeroMonthFinishedDate !== "undefined" ) {
                dates.zeroMonth = data.zeroMonthFinishedDate;
                res.status(200).send(dates);
            } else {
                res.status(200).send("undefined");
            }
        } else {
            res.status(200).send("No Date Found");
        }
    });
});

app.post('/getOneFinishDate', (req, res) => {
    Account.findOne({ _id:req.body._id }, (err, data) => {
        if(data !== "" || data !== null || data!== undefined){    
            let dates = { "oneMonth":"" };
            if( data.oneMonthFinishedDate !== "undefined" ) {
                dates.oneMonth = data.oneMonthFinishedDate;
                res.status(200).send(dates);
            } else {
                res.status(200).send("undefined");
            }
        } else {
            res.status(200).send("No Date Found");
        }
    });
});

app.post('/getThreeFinishDate', (req, res) => {
    Account.findOne({ _id:req.body._id }, (err, data) => {
        if(data !== "" || data !== null || data!== undefined) {
            let dates = { "threeMonth":"" };
            if( data.threeMonthFinishedDate !== "undefined" ) { 
                dates.threeMonth = data.threeMonthFinishedDate;
                res.status(200).send(dates);
            } else {
                res.status(200).send("undefined");
            }
        } else {
            res.status(200).send("No Date Found");
        }
    });
});

app.post('/getSixFinishDate', (req, res) => {
    Account.findOne({ _id:req.body._id }, (err, data) => {
        let dates = { "sixMonth":"" };
        if( data.sixMonthFinishedDate !== "undefined" ) {
            dates.sixMonth = data.sixMonthFinishedDate;
            res.status(200).send(dates);
        } else {
            res.status(200).send("undefined");
        } 
    });
});

app.post('/test/zero', (req, res) => {
    Questionries_zero.find((err, result) => {
        res.status(200).send(result);
    });
});

app.get('/', (req, res) => {
    console.log('reached here from heroku');
    res.send('Hello World');
});

app.get('/getZeroQuestions', (req, res) => {
    mongoose.connection.db.collection('question_0', (err, collection) => {
        let cur = collection.find();
        cur.on('data', (doc) => { res.send(doc) });
        cur.on('end', () => { console.log('done'); });
    });
});

app.get('/getOneQuestions', (req, res) => {
    mongoose.connection.db.collection('question_1_month', (err, collection) => {
        let cur = collection.find();
        cur.on('data', (doc) => { res.send(doc) });
        cur.on('end', () => { console.log('done'); });
    });
});

app.get('/getThreeQuestions', (req, res) => {
    mongoose.connection.db.collection('question_3_month', (err, collection) => {
        let cur = collection.find();
        cur.on('data', (doc) => { res.send(doc) });
        cur.on('end', () => { console.log('done'); });
    });
});

app.get('/getSixQuestions', (req, res) => {
    mongoose.connection.db.collection('question_6_month', (err, collection) => {
        let cur = collection.find();
        cur.on('data', (doc) => { res.send(doc) });
        cur.on('end', () => { console.log('done'); });
    });
});

app.get('/getZeroAnswers', (req, res) => {
    mongoose.connection.db.collection('questionries_zeros', (err, collection) => {
        let cur = collection.find();
        cur.on('data', (doc) => { res.send(doc) });
        cur.on('end', () => { console.log('done'); });
    });
});

app.get('/getOneAnswers', (req, res) => {
    mongoose.connection.db.collection('questionries_ones', (err, collection) => {
        let cur = collection.find();
        cur.on('data', (doc) => { res.send(doc) });
        cur.on('end', () => { console.log('done'); });
    });
});

app.get('/getThreeAnswers', (req, res) => {
    mongoose.connection.db.collection('questionries_threes', (err, collection) => {
        let cur = collection.find();
        cur.on('data', (doc) => { res.send(doc) });
        cur.on('end', () => { console.log('done'); });
    });
});

app.get('/getSixAnswers', (req, res) => {
    mongoose.connection.db.collection('questionries_sixes', (err, collection) => {
        let cur = collection.find();
        cur.on('data', (doc) => { res.send(doc) });
        cur.on('end', () => { console.log('done'); });
    });
});

app.post('/saveEmail', (req, res) => {
    console.log(req.body)
    Questionries_zero.findOneAndUpdate({ _id:req.body._id }, {
        section_10: {
            q1_id: req.body.q1_id,
            q1_ans:req.body.q1_ans,
            q2_id: req.body.q2_id,
            q2_ans: req.body.q2_ans,
            q3_id: req.body.q3_id,
            q3_ans: req.body.q3_ans,
            q4_id: req.body.q4_id,
            q4_ans: req.body.q4_ans,
            q5_id: req.body.q5_id,
            q5_ans: req.body.q5_ans,
            q6_id: req.body.q6_id,
            q6_ans: req.body.q6_ans,
            q7_id: req.body.q7_id,
            q7_ans: req.body.q7_ans,
            q8_id: req.body.q8_id,
            q8_ans: req.body.q8_ans,
            q9_id: req.body.q9_id,
            q9_ans: req.body.q9_ans,
            q10_id: req.body.q10_id,
            q10_ans: req.body.q10_ans,
            q11_id: req.body.q11_id,
            q11_ans: req.body.q11_ans,
            email: req.body.email
        }
    }, 
    (err, doc) => {
        res.status(200).send(doc);
    });
});

app.get('/getEmail', (req, res) => {
    Questionries_zero.find((err, doc) => {
        if(doc !== " " || doc !== null || doc !== "undefined") {
            let servData = [];
            doc.forEach( data => {
                servData.push(data.section_10);
            });
            res.status(200).send(servData);
        }
    });
});

module.exports = app;