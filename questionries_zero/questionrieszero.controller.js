﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const questionzeroService = require('./questionrieszero.service');

// routes
router.post('/iniQuestionaries', iniQuestionariesSaveDataSchema, iniQuestionaries);
router.post('/section1SaveData', section1SaveDataSchema, section1SaveData);
router.post('/section2SaveData', section2SaveDataSchema, section2SaveData);
router.post('/section3SaveData', section3SaveDataSchema, section3SaveData);
router.post('/section4SaveData', section4SaveDataSchema, section4SaveData);
router.post('/section5SaveData', section5SaveDataSchema, section5SaveData);
router.post('/section6SaveData', section6SaveDataSchema, section6SaveData);
router.post('/section7SaveData', section7SaveDataSchema, section7SaveData);
router.post('/section8SaveData', section8SaveDataSchema, section8SaveData);
router.post('/section9SaveData', section9SaveDataSchema, section9SaveData);
router.post('/section10SaveData', section10SaveDataSchema, section10SaveData);
// router.post('/sendEmailOnCompletion', sendEmailOnCompletionSchema, sendEmailOnCompletion, finishDateSchema, addfinishDate);
router.post('/getCode', getCode);
router.post('/sendGift', sendGift);



module.exports = router;

function iniQuestionaries(req, res, next) {
    console.log(req.body);
    questionzeroService.section1SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}

function section1SaveData(req, res, next) {
 //   console.log(req.body);
    questionzeroService.section1SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section2SaveData(req, res, next) {
 //   console.log(req.body);
    questionzeroService.section2SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}

function section3SaveData(req, res, next) {
    console.log(req.body);
    questionzeroService.section3SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section4SaveData(req, res, next) {
    console.log(req.body);
    questionzeroService.section4SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section5SaveData(req, res, next) {
    console.log(req.body);
    questionzeroService.section5SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section6SaveData(req, res, next) {
    console.log(req.body);
    questionzeroService.section6SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section7SaveData(req, res, next) {
    console.log(req.body);
    questionzeroService.section7SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section8SaveData(req, res, next) {
    console.log(req.body);
    questionzeroService.section8SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section9SaveData(req, res, next) {
    console.log(req.body);
    questionzeroService.section9SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
function section10SaveData(req, res, next) {
    console.log(req.body);
    questionzeroService.section10SaveData(req.body, req.get('origin'))
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}

function addfinishDate(req, res, next) {
    console.log(req.body);
    questionzeroService.addMonthFinishDate(req.body)
        .then(() => res.json({ status:'done',message: 'Data Inserted' }))
        .catch(next);
}
 
function sendEmailOnCompletion(req, res, next) {
    console.log(req.body);
    questionzeroService.sendEmailOnCompletion(req.body, req.get('origin'))
    .then((rec) =>  res.json(rec))
    .catch(next);
 }
 function getCode(req, res, next) {
    questionzeroService.getCode(req.body, req.get('origin'))
    .then((rec) =>  res.json(rec))
    .catch(next);
 }

 function sendGift(req, res, next) {
    // console.log('iam here');
    // console.log(req.body);
    questionzeroService.sendGift(req.body, req.get('origin'))
    .then((rec) =>  res.json(rec))
    .catch(next);
 }
//===================================================================================================

function iniQuestionariesSaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required()   
    });
    validateRequest(req, next, schema);
}

function section1SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required(),
        q3_id: Joi.number().required(),
        q3_ans: Joi.string().required(),
    });
    validateRequest(req, next, schema);
}
function section2SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
         q3_id: Joi.number().required(),
    });
    validateRequest(req, next, schema);
}
function section3SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.number().required(),
        q3_id: Joi.number().required(),
        q3_ans: Joi.number().required(),
        q4_id: Joi.number().required(),
        q4_ans: Joi.number().required(),
        q5_id: Joi.number().required(),
        q5_ans: Joi.number().required(),
        q6_id: Joi.number().required(),
        q6_ans: Joi.number().required(),
        q7_id: Joi.number().required(),
        q7_ans: Joi.number().required(),
        q8_id: Joi.number().required(),
        q8_ans: Joi.number().required(),
        q9_id: Joi.number().required(),
        q9_ans: Joi.number().required(),
        q10_id: Joi.number().required(),
        q10_ans: Joi.number().required(),
        q11_id: Joi.number().required(),
        q11_ans: Joi.number().required()
    });
    validateRequest(req, next, schema);
}

function section4SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.number().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.number().required(),
        q3_id: Joi.number().required(),
        q3_ans: Joi.number().required(),
        q4_id: Joi.number().required(),
        q4_ans: Joi.number().required()
    });
    validateRequest(req, next, schema);
}


function section5SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.number().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.number().required()
    });
    validateRequest(req, next, schema);
}

function section6SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required(),
        q3_id: Joi.number().required(),
        q3_ans: Joi.string().required(),

        q4_id: Joi.number().required(),
        q4_ans: Joi.string().required(),
        q5_id: Joi.number().required(),
        q5_ans: Joi.string().required(),
        q6_id: Joi.number().required(),
        q6_ans: Joi.string().required(),

        q7_id: Joi.number().required(),
        q7_ans: Joi.string().required(),
        q8_id: Joi.number().required(),
        q8_ans: Joi.string().required(),
        q9_id: Joi.number().required(),
        q9_ans: Joi.string().required(),

        q10_id: Joi.number().required(),
        q10_ans: Joi.string().required()

   
        
    });
    validateRequest(req, next, schema);
}

function section7SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.number().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.number().required(),
        q3_id: Joi.number().required(),
        q3_ans: Joi.number().required()

   
        
    });
    validateRequest(req, next, schema);
}

function section8SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required(),
        q3_id: Joi.number().required(),
        q4_ans: Joi.string().required()

   
        
    });
    validateRequest(req, next, schema);
}

function section9SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required(),
        q3_id: Joi.number().required(),
        q4_ans: Joi.string().required()

   
        
    });
    validateRequest(req, next, schema);
}

function section10SaveDataSchema(req, res, next) {
    const schema = Joi.object({
        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
        q2_id: Joi.number().required(),
        q2_ans: Joi.string().required(),
        q3_id: Joi.number().required(),
        q3_ans: Joi.string().required(),
        q4_id: Joi.number().required(),
        q4_ans: Joi.string().required(),
        q5_id: Joi.number().required(),
        q5_ans: Joi.string().required(),
        q6_id: Joi.number().required(),
        q6_ans: Joi.string().required(),
        q7_id: Joi.number().required(),
        q7_ans: Joi.string().required(),
        q8_id: Joi.number().required(),
        q8_ans: Joi.string().required(),
        q9_id: Joi.number().required(),
        q9_ans: Joi.string().required(),
        q10_id: Joi.number().required(),
        q10_ans: Joi.string().required(),
        q11_id: Joi.number().required(),
        q11_ans: Joi.string().required()
    });
    validateRequest(req, next, schema);
}

function finishDateSchema(req, res, next) {
    console.log('req.body : ',req.body);
    const schema = Joi.object({
        _id: Joi.string().required(),
        zeroMonthFinishedDate: Joi.string().required()                
    });
    validateRequest(req, next, schema);
}

function sendEmailOnCompletionSchema(req, res, next) {
    console.log('req.body : ',req.body);
    const schema = Joi.object({
        _id: Joi.string().required(),
        name: Joi.string().required(),
        email: Joi.string().required(),                
    });
    validateRequest(req, next, schema);
}