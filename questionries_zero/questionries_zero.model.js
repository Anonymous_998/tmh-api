const { number } = require('joi');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
   
    _id: { type: String, required: true,trim: true},

    section_1:{ 
        q1_id: { type: String,trim: true},
        q1_ans: { type: String,trim: true},
        q2_id: { type: String, trim: true},
        q2_ans: { type: String,trim: true},
        q3_id:{ type: String, trim: true},
        q3_ans: { type: String,trim: true}
      
    },
    section_2:{ 
        q1_id: { type: String,trim: true},
        q1_ans: { type: String,trim: true},
        q2_id: { type: String, trim: true},
        q2_ans: { type: String,trim: true},
        q3_id:{ type: String, trim: true},
        q3_ans: { type: String,trim: true}
    },
    section_3:{ 
        q1_id: { type: String,trim: true},
        q1_ans: { type: String,trim: true},
        q2_id: { type: String, trim: true},
        q2_ans: { type: String,trim: true},
        q3_id:{ type: String, trim: true},
        q3_ans: { type: String,trim: true}
    },
 
     section_4:{ 
         q1_id: { type: String,trim: true},
         q1_ans: { type: String,trim: true},
         q2_id: { type: String, trim: true},
         q2_ans: { type: String,trim: true},
         q3_id:{ type: String, trim: true},
         q3_ans: { type: String,trim: true},
         q4_id: { type: String,trim: true},
         q4_ans: { type: String,trim: true}
      },
      section_5:{ 
          q1_id: { type: String,trim: true},
          q1_ans: { type: String,trim: true},
          q2_id: { type: String, trim: true},
          q2_ans: { type: String,trim: true}
       },
       section_6:{ 
        q1_id: { type: String,trim: true},
        q1_ans: { type: String,trim: true},
        q2_id: { type: String, trim: true},
        q2_ans: { type: String,trim: true},
        q3_id:{ type: String, trim: true},
        q3_ans: { type: String,trim: true},
        q4_id: { type: String,trim: true},
        q4_ans: { type: String,trim: true},
        q5_id: { type: String,trim: true},
        q5_ans: { type: String,trim: true},
        q6_id: { type: String, trim: true},
        q6_ans: { type: String,trim: true},
        q7_id:{ type: String, trim: true},
        q7_ans: { type: String,trim: true},
        q8_id: { type: String,trim: true},
        q8_ans: { type: String,trim: true},
        q9_id: { type: String,trim: true},
        q9_ans: { type: String,trim: true},
        q10_id: { type: String, trim: true},
        q10_ans: { type: String,trim: true}
        },
        section_7:{ 
            q1_id: { type: Number,trim: true},
            q1_ans: { type: Number,trim: true},
            q2_id: { type: Number, trim: true},
            q2_ans: { type: Number,trim: true},
            q3_id:{ type: Number, trim: true},
            q3_ans: { type: Number,trim: true},

         },
         section_8:{ 
             q1_id: { type: String,trim: true},
             q1_ans: { type: String,trim: true},
             q2_id: { type: String, trim: true},
             q2_ans: { type: String,trim: true},
             q3_id:{ type: String, trim: true},
             q3_ans: { type: String,trim: true},
             q4_id: { type: String,trim: true},
             q4_ans: { type: String,trim: true}
          },
    section_9:{ 
        q1_id: { type: String,trim: true},
        q1_ans: { type: String,trim: true},
        q2_id: { type: String, trim: true},
        q2_ans: { type: String,trim: true},
        q3_id:{ type: String, trim: true},
        q3_ans: { type: String,trim: true},
        q4_id: { type: String,trim: true},
        q4_ans: { type: String,trim: true}
     },
                 
     section_10:{ 
        q1_id: { type: String,trim: true},
        q1_ans: { type: String,trim: true},
        q2_id: { type: String, trim: true},
        q2_ans: { type: String,trim: true},
        q3_id:{ type: String, trim: true},
        q3_ans: { type: String,trim: true},
        q4_id: { type: String,trim: true},
        q4_ans: { type: String,trim: true},
        q5_id: { type: String,trim: true},
        q5_ans: { type: String,trim: true},
        q6_id: { type: String, trim: true},
        q6_ans: { type: String,trim: true},
        q7_id:{ type: String, trim: true},
        q7_ans: { type: String,trim: true},
        q8_id: { type: String,trim: true},
        q8_ans: { type: String,trim: true},
        q9_id: { type: String,trim: true},
        q9_ans: { type: String,trim: true},
        q10_id: { type: String, trim: true},
        q10_ans: { type: String,trim: true},
        q11_id:{ type: String, trim: true},
        q11_ans: { type: String,trim: true},
        email: { type: String,trim: true}
     }

});


module.exports = mongoose.model('Questionries_zero', schema);
