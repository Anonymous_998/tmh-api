
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    block_identifier: { type: Number },
    block_size: { type: Number, required: true },
    sequence_within_block: { type: Number, required: true },
    treatment: { type: String, required: true },
    code: { type: String, required: true },
    used: { type: Number, required: true }

});



module.exports = mongoose.model('Randomization', schema);