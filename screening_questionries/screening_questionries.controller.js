﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const ScreeningService = require('./screeningquestionries.service');

// routes

router.post('/section1SaveData', section1SaveDataSchema, section1SaveData);
router.post('/section2SaveData', section2SaveDataSchema, section2SaveData);
router.post('/section3SaveData', section3SaveDataSchema, section3SaveData);
router.post('/section4SaveData', section4SaveDataSchema, section4SaveData);
router.post('/section5SaveData', section5SaveDataSchema, section5SaveData);
router.post('/section6SaveData', section6SaveDataSchema, section6SaveData);
router.post('/section7SaveData', section7SaveDataSchema, section7SaveData);
router.post('/section8SaveData', section8SaveDataSchema, section8SaveData);
router.post('/section9SaveData', section9SaveDataSchema, section9SaveData);
router.post('/section10SaveData', section10SaveDataSchema, section10SaveData);
router.post('/section11SaveData', section11SaveDataSchema, section11SaveData);
router.post('/section12SaveData', section12SaveDataSchema, section12SaveData);
router.post('/section13SaveData', section13SaveDataSchema, section13SaveData);
router.post('/section14SaveData', section14SaveDataSchema, section14SaveData);




module.exports = router;

function section1SaveData(req, res, next) {
      console.log(req.body);
       ScreeningService.section1SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }

   function section2SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section2SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section3SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section3SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section4SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section4SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section5SaveData(req, res, next) {
      console.log('Amir');
       ScreeningService.section5SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section6SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section6SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section7SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section7SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section8SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section8SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section9SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section9SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section10SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section10SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section11SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section11SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }
   function section12SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section12SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }

   function section13SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section13SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }

   function section14SaveData(req, res, next) {
    //   console.log(req.body);
       ScreeningService.section14SaveData(req.body, req.get('origin'))
           .then(() => res.json({ status:'done',message: 'Data Inserted' }))
           .catch(next);
   }

   //=====================
   function section1SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}
function section2SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}
function section3SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}
function section4SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}
function section5SaveDataSchema(req, res, next) {
  const schema = Joi.object({

      _id: Joi.string().required(),
      q1_id: Joi.number().required(),
      q1_ans: Joi.string().required(),
    });
  validateRequest(req, next, schema);
}
function section6SaveDataSchema(req, res, next) {
  const schema = Joi.object({

      _id: Joi.string().required(),
      q1_id: Joi.number().required(),
      q1_ans: Joi.string().required(),
    });
  validateRequest(req, next, schema);
}
function section7SaveDataSchema(req, res, next) {
  const schema = Joi.object({

      _id: Joi.string().required(),
      q1_id: Joi.number().required(),
      q1_ans: Joi.string().required(),
    });
  validateRequest(req, next, schema);
}
function section8SaveDataSchema(req, res, next) {
  const schema = Joi.object({

      _id: Joi.string().required(),
      q1_id: Joi.number().required(),
      q1_ans: Joi.string().required(),
    });
  validateRequest(req, next, schema);
}
function section9SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}
function section10SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}
function section11SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}
function section12SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}
function section13SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}
function section14SaveDataSchema(req, res, next) {
    const schema = Joi.object({

        _id: Joi.string().required(),
        q1_id: Joi.number().required(),
        q1_ans: Joi.string().required(),
      });
    validateRequest(req, next, schema);
}