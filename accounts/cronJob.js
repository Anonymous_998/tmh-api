const config = require('config.json');
const sendEmail = require('_helpers/send-email');
const db = require('_helpers/db');


function getTodayDate(){
    var d = new Date();
    
    var date = d.getDate();
    var month = d.getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12
    var year = d.getFullYear();
        
    return date + "/" + month + "/" + year;
}
var date_diff_indays = function(date1, date2) {
    dt1 = new Date(date1);
    dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }
   // console.log(date_diff_indays('04/02/2014', '11/04/2014'));
 //   console.log(date_diff_indays('11/04/2014','12/02/2014'));

async function cronOne(origin) {
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1  });
   console,log
    acc.forEach(account => { 
    if(account.zeroMonth==1 && account.oneMonth==0){
        var dt1=account.zeroMonthFinishedDate;
        var dt2=getTodayDate();
        var dateDif= date_diff_indays(dt1,dt2);
        if(dateDif < 4 &&  dateDif > 0){

            let message;
    const loginUrl = 'http://www.trialmyapp.ca/#/signin';
message = '<p> Hi</p>';
message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     

sendEmail({
        to: account.email,
        subject: 'First Month Questionnaire Reminder',
        html: `<p></p>
               ${message}`
    });


        }
    
    }

    });
}

async function cronTwo(origin) {
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1  });
    acc.forEach(account => { 
    if(account.oneMonth==1 && account.threeMonth==0){
        var dt1=account.oneMonthFinishedDate;
        var dt2=getTodayDate();
        var dateDif= date_diff_indays(dt1,dt2);
        if(dateDif < 4 &&  dateDif > 0){

            let message;
    const loginUrl = 'http://www.trialmyapp.ca/#/signin';
message = '<p> Hi</p>';
message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     

sendEmail({
        to: account.email,
        subject: 'Third Month Questionnaire Reminder',
        html: `<p></p>
               ${message}`
    });


        }
    
    }

    });
}

async function cronThree(origin) {
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1  });
    acc.forEach(account => { 
    if(account.threeMonth==1 && account.sixMonth==0){
        var dt1=account.threeMonthFinishedDate;
        var dt2=getTodayDate();
        var dateDif= date_diff_indays(dt1,dt2);
        if(dateDif < 4 &&  dateDif > 0){

            let message;
    const loginUrl = 'http://www.trialmyapp.ca/#/signin';
message = '<p> Hi</p>';
message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     

sendEmail({
        to: account.email,
        subject: 'Six Month Questionnaire Reminder',
        html: `<p></p>
               ${message}`
    });


        }
    
    }

    });
}