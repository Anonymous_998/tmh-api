﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require("crypto");
const sendEmail = require('_helpers/send-email');
const db = require('_helpers/db');
const Role = require('_helpers/role');
const request = require('request');
const http  = require('http');
const Swal = require('sweetalert2');

module.exports = {
    authenticate,
    refreshToken,
    revokeToken,
    register,
    gmail,
    verifyEmail,
    forgotPassword,
    validateResetToken,
    resetPassword,
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    eligibility,
    leaveStudy,
    cronOneMonth,
    cronThreeMonth,
    cronSixMonth,
    cronOne,
    cronTwo,
    cronThree,
    cronSix,
    test
};

async function test(data) {
    const data1 = await "20";
    console.log('iam working', data,' ',data1);
}

async function authenticate({ email, password, ipAddress }) {
    const account = await db.Account.findOne({ email });
    
    if (!account || !account.isVerified || !bcrypt.compareSync(password, account.passwordHash)) {
        throw 'Email or password is incorrect';
    }

    
    if (account.eligibility==0 ) {
        throw 'Your are Ineligible for this study';
    } 

  
   
    
    if (account.leaveStudy==1 ) {
        throw 'You can not sign in because you left study';
    } 

    // authentication successful so generate jwt and refresh tokens
    const jwtToken = generateJwtToken(account);
    const refreshToken = generateRefreshToken(account, ipAddress);

    // save refresh token
    await refreshToken.save();

    // return basic details and tokens
    return {
        ...basicDetails(account),
        jwtToken,
        refreshToken: refreshToken.token
    };
}

async function refreshToken({ token, ipAddress }) {
    const refreshToken = await getRefreshToken(token);
    const { account } = refreshToken;

    // replace old refresh token with a new one and save
    const newRefreshToken = generateRefreshToken(account, ipAddress);
    refreshToken.revoked = Date.now();
    refreshToken.revokedByIp = ipAddress;
    refreshToken.replacedByToken = newRefreshToken.token;
    await refreshToken.save();
    await newRefreshToken.save();

    // generate new jwt
    const jwtToken = generateJwtToken(account);

    // return basic details and tokens
    return {
        ...basicDetails(account),
        jwtToken,
        refreshToken: newRefreshToken.token
    };
}

async function revokeToken({ token, ipAddress }) {
    const refreshToken = await getRefreshToken(token);

    // revoke token and save
    refreshToken.revoked = Date.now();
    refreshToken.revokedByIp = ipAddress;
    await refreshToken.save();
}

async function register(params, origin) {
    // validate
    if (await db.Account.findOne({ email: params.email })) {
        // send already registered error in email to prevent account enumeration
         await sendAlreadyRegisteredEmail(params.email, origin);

        return { 
            status:'error',
            msg:"Acount is already register with this email",
    };
    }

    // create account object
    const account = new db.Account(params);

    // first registered account is an admin
    const isFirstAccount = (await db.Account.countDocuments({})) === 0;
    account.role = isFirstAccount ? Role.Admin : Role.User;
    account.verificationToken = randomTokenString();

    // hash password
    account.passwordHash = hash(params.password);

    // save account
    await account.save();

    // send email
    await sendVerificationEmail(account, origin);
}


async function gmail(params, origin,ipAddress) {
    // validate
    const acc=await db.Account.findOne({ email: params.email })
// console.log(acc);
    if (acc) {
        // send already registered error in email to prevent account enumeration
      //  return await sendAlreadyRegisteredEmail(params.email, origin);

      if (acc.eligibility==0 ) {
        throw 'Your are Ineligible for this study';
    } 

  
   
    
    if (acc.leaveStudy==1 ) {
        throw 'You can not sign in because you left study';
    } 

        // login karo
        const account = new db.Account(params);

        const jwtToken = generateJwtToken(db.Account);
        const refreshToken = generateRefreshToken(db.Account, ipAddress);
    
        // save refresh token
        await refreshToken.save();
    
        // return basic details and tokens
        return {
            ...basicDetails(account),
            jwtToken,
            refreshToken: refreshToken.token
        };

    }

    // create account object
    const account = new db.Account(params);

    // first registered account is an admin
    const isFirstAccount = (await db.Account.countDocuments({})) === 0;
    account.role = isFirstAccount ? Role.Admin : Role.User;
    account.verificationToken = randomTokenString();

    // hash password
    account.passwordHash = hash(params.password);

    // save account
    await account.save();

    const jwtToken = generateJwtToken(account);
    const refreshToken = generateRefreshToken(account, ipAddress);

    // save refresh token
    await refreshToken.save();

    // return basic details and tokens
    return {
        ...basicDetails(account),
        jwtToken,
        refreshToken: refreshToken.token
    };

   
}

async function verifyEmail({ token }) {
    const account = await db.Account.findOne({ verificationToken: token });

    if (!account) throw 'Verification failed';

    account.verified = Date.now();
    account.verificationToken = undefined;
    await account.save();
}

async function forgotPassword({ email }, origin) {
    const account = await db.Account.findOne({ email });

    // always return ok response to prevent email enumeration
    if (!account) return;

    // create reset token that expires after 24 hours
    account.resetToken = {
        token: randomTokenString(),
        expires: new Date(Date.now() + 24*60*60*1000)
    };
    await account.save();

    // send email
    await sendPasswordResetEmail(account, origin);
}

async function validateResetToken({ token }) {
    const account = await db.Account.findOne({
        'resetToken.token': token,
        'resetToken.expires': { $gt: Date.now() }
    });

    if (!account) throw 'Invalid token';
}

async function resetPassword({ token, password }) {
    const account = await db.Account.findOne({
        'resetToken.token': token,
        'resetToken.expires': { $gt: Date.now() }
    });

    if (!account) throw 'Invalid token';

    // update password and remove reset token
    account.passwordHash = hash(password);
    account.passwordReset = Date.now();
    account.resetToken = undefined;
    await account.save();
}

async function getAll() {
    const accounts = await db.Account.find();
    return accounts.map(x => basicDetails(x));
}

async function getById(id) {
    const account = await getAccount(id);
    return basicDetails(account);
}

async function create(params) {
    // validate
    if (await db.Account.findOne({ email: params.email })) {
        throw 'Email "' + params.email + '" is already registered';
    }

    const account = new db.Account(params);
    account.verified = Date.now();

    // hash password
    account.passwordHash = hash(params.password);

    // save account
    await account.save();

    return basicDetails(account);
}

async function update(id, params) {
    const account = await getAccount(id);

    // validate (if email was changed)
    if (params.email && account.email !== params.email && await db.Account.findOne({ email: params.email })) {
        throw 'Email "' + params.email + '" is already taken';
    }

    // hash password if it was entered
    if (params.password) {
        params.passwordHash = hash(params.password);
    }

    // copy params to account and save
    Object.assign(account, params);
    account.updated = Date.now();
    await account.save();

    return basicDetails(account);
}

async function _delete(id) {
    const account = await getAccount(id);
    await account.remove();
}

// helper functions

async function getAccount(id) {
    if (!db.isValidId(id)) throw 'Account not found';
    const account = await db.Account.findById(id);
    if (!account) throw 'Account not found';
    return account;
}

async function getRefreshToken(token) {
    const refreshToken = await db.RefreshToken.findOne({ token }).populate('account');
    if (!refreshToken || !refreshToken.isActive) throw 'Invalid token';
    return refreshToken;
}

function hash(password) {
    return bcrypt.hashSync(password, 10);
}

function generateJwtToken(account) {
    // create a jwt token containing the account id that expires in 15 minutes
    return jwt.sign({ sub: account.id, id: account.id }, config.secret, { expiresIn: '15m' });
}

function generateRefreshToken(account, ipAddress) {
    // create a refresh token that expires in 7 days
    return new db.RefreshToken({
        account: account.id,
        token: randomTokenString(),
        expires: new Date(Date.now() + 7*24*60*60*1000),
        createdByIp: ipAddress
    });
}

function randomTokenString() {
    return crypto.randomBytes(40).toString('hex');
} 

function basicDetails(account) {
    const { id, title, firstName, lastName, email, role, created, updated, isVerified,randomization_code, treatment,zeroMonth,zeroMonthFinishedDate,oneMonth,oneMonthFinishedDate,ThreeMonth,ThreeMonthFinishedDate,sixMonth,sixMonthFinishedDate } = account;
    return { id, title, firstName, lastName, email, role, created, updated, isVerified,randomization_code, treatment,zeroMonth,zeroMonthFinishedDate,oneMonth,oneMonthFinishedDate,ThreeMonth,ThreeMonthFinishedDate,sixMonth,sixMonthFinishedDate };
}

async function sendVerificationEmail(account, origin) {
    let message;
    if (origin) {
        const verifyUrl = `${origin}/#/accounts/Verify-email/${account.verificationToken}`;
        message = `<p>Please click the below link to verify your email address:</p>
                   <p><a href="${verifyUrl}">${verifyUrl}</a></p>`;
    } else {
        message = `<p>Please use the below token to verify your email address with the <code>/Verify_email</code> api route:</p>
                   <p><code>${account.verificationToken}</code></p>`;
    }

    await sendEmail({
        to: account.email,
        subject: 'Trial My App - Account Verification Notification',
        html: `<h4>Verify Email</h4>
               <p>Thanks for registering!</p>
               ${message}`
    });
}

async function sendAlreadyRegisteredEmail(email, origin) {
    let message;
    if (origin) {
        message = `<p>If you don't know your password please visit the <a href="${origin}/#/account/forgot-password">forgot password</a> page.</p>`;
    } else {
        message = `<p>If you don't know your password you can reset it via the <code>/account/forgot-password</code> api route.</p>`;
    }

    await sendEmail({
        to: email,
        subject: 'Trial My App - Email Already Registered',
        html: `<h4>Email Already Registered</h4>
               <p>Your email <strong>${email}</strong> is already registered.</p>
               ${message}`
    });

  
}

async function sendPasswordResetEmail(account, origin) {
    let message;
    if (origin) {
        const resetUrl = `${origin}/#/account/Reset_account_password/${account.resetToken.token}`;
        message = `<p>Please click the below link to reset your password, the link will be valid for 1 day:</p>
                   <p><a href="${resetUrl}">${resetUrl}</a></p>`;
    } else {
        message = `<p>Please use the below token to reset your password with the <code>/account/reset-password</code> api route:</p>
                   <p><code>${account.resetToken.token}</code></p>`;
    }

    await sendEmail({
        to: account.email,
        subject: 'Trial My App - Reset Password Instructions',
        html: `<h4>Reset Password Email</h4>
               ${message}`
    });
}

async function eligibility(params) {
    console.log(params.id);
    const _id=params.id;
    const rec =  db.Account.findByIdAndUpdate({_id},{"eligibility": 0},function(err, result){

        if(err){  console.log(err);  } else { console.log(result);  }

    });

}

async function leaveStudy(params) {
    console.log(params.id);
    const _id=params.id;
    const rec =  db.Account.findByIdAndUpdate({_id},{"leaveStudy": 1},function(err, result){

        if(err){  console.log(err);  } else { console.log(result);  }

    });

}





async function cronOneMonth(origin) {
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1  });
    // console.log(acc);
    acc.forEach(account => { 
    // console.log(account);
    let message;
    const loginUrl = 'http://www.trialmyapp.ca/#/signin';
message = '<p> Hi</p>';
message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     

sendEmail({
        to: account.email,
        subject: 'First Month Questionnaire Reminder',
        html: `<p></p>
               ${message}`
    });

}); 
}
async function cronThreeMonth(origin) {
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1  });
    acc.forEach(account => { 
    // console.log(account);
    let message;
    const loginUrl = 'http://www.trialmyapp.ca/#/signin';
message = '<p> Hi</p>';
message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 or 3 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     

sendEmail({
        to: account.email,
        subject: 'Third Month Questionnaire Reminder',
        html: `<p></p>
               ${message}`
    });

}); 
}
async function cronSixMonth(origin) {
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1  });
    acc.forEach(account => { 
    // console.log(account);
    let message;
    const loginUrl = 'http://www.trialmyapp.ca/#/signin';
message = '<p> Hi</p>';
message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 or 3 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     

sendEmail({
        to: account.email,
        subject: 'Six Month Questionnaire Reminder',
        html: `<p></p>
               ${message}`
    });

}); 
}
//==================== Cron Jobs ==============================

function getTodayDate(){
    var d = new Date();
    var date = d.getDate();
    var month = d.getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12
    var year = d.getFullYear();
    let filDate = [year, month, date];

    return filDate;
}
var date_diff_indays = function(date1, date2) {
    let date = date1;
    if( date !== undefined ) {
        date = date.split('-');
    }
        dt1 = new Date(date[0], date[1], date[2]);
        dt2 = new Date(date2[0], date2[1], date2[2]);
    // console.log(dt1,' ',dt2);
    // console.log(Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24)));
    return Math.abs((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
}
   // console.log(date_diff_indays('04/02/2014', '11/04/2014'));
 //   console.log(date_diff_indays('11/04/2014','12/02/2014'));

async function cronOne(origin) {
    let emailSent = false;
    var d = new Date();
    var date = d.getDate();
    var month = d.getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12
    var year = d.getFullYear();
    let filDate = [year, month, date];
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1 });
    // console.log(acc);
//    console.log(origin);
    acc.forEach(account => { 
    if( account.zeroMonth == 1 && account.oneMonth == 0 ) {
        var dt1 = "";
        if(account.zeroMonthFinishedDate) {
            dt1 = account.zeroMonthFinishedDate;
        }
        // let date = dt1.split('-');
        // console.log(dt1);
        // console.log(date[0],' ',date[1],' ',date[2]);
        var dt2 = getTodayDate();
        // console.log(dt2);
        var dateDif = date_diff_indays(dt1,dt2);
        // console.log(dateDif);
        if( dateDif === 29 || dateDif === 32 || dateDif === 35 ) {

            if( emailSent === false ) {
                console.log('sending email');
                let message;
                const loginUrl = 'http://www.trialmyapp.ca/#/signin';
                message = '<p> Hi</p>';
                message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
                message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
                message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
                message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia Lokker at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     

                sendEmail({
                        to: account.email,
                        subject: 'First Month Questionnaire Reminder',
                        html: `<p></p>
                            ${message}`
                    });

                console.log('Email sent');
                emailSent = true;
            } else {
                console.log( 'Email Already Sent' );
            }

        } else {
            // console.log('Still we have time to send it....');
        }
    
    }

    });
}

async function cronTwo(origin) {
    let emailSent = false;
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1  });
    // console.log(acc);
    acc.forEach(account => { 
    if(account.oneMonth==1 && account.threeMonth==0 && account.oneMonthFinishedDate){
        var dt1=account.oneMonthFinishedDate;
        var dt2=getTodayDate();
        var dateDif= date_diff_indays(dt1,dt2);

        if( dateDif === 89 || dateDif === 92 || dateDif === 95 ){

            if( emailSent === false ) {
                let message;
                const loginUrl = 'http://www.trialmyapp.ca/#/signin';
                message = '<p> Hi</p>';
                message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
                message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
                message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
                message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     

                sendEmail({
                        to: account.email,
                        subject: 'Third Month Questionnaire Reminder',
                        html: `<p></p>
                            ${message}`
                    });
            } else {
                console.log( 'Your Already recieved month 3 remainder email' );
            }

        }
    
    }

    });
}

async function cronThree(origin) {
    let emailSent = false;
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1  });
    // console.log(acc);
    acc.forEach(account => { 
    if(account.threeMonth==1 && account.sixMonth==0 && account.threeMonthFinishedDate){
        var dt1=account.threeMonthFinishedDate;
        var dt2=getTodayDate();
        var dateDif= date_diff_indays(dt1,dt2);

        if( dateDif === 179 || dateDif === 182 || dateDif === 185 ){

            if( emailSent === false ) {
                let message;
                const loginUrl = 'http://www.trialmyapp.ca/#/signin';
                message = '<p> Hi</p>';
                message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
                message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
                message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
                message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     
    
                sendEmail({
                        to: account.email,
                        subject: 'Six Month Questionnaire Reminder',
                        html: `<p></p>
                            ${message}`
                    });
            } else {
                console.log('you already got month 6 remainder email');
            }

        }
    
    }

    });
} 

async function cronSix(origin) {
    let emailSent = false;
    const acc = await db.Account.find({role:'User',leaveStudy:0,eligibility:1  });
    // console.log(acc);
    acc.forEach(account => { 
    if( account.sixMonth == 0 && account.sixMonthFinishedDate != undefined ){
        var dt1=account.sixMonthFinishedDate;
        var dt2=getTodayDate();
        var dateDif= date_diff_indays(dt1,dt2);

        if(dateDif === 366){

            if(emailSent === false) {
                let message;
                const loginUrl = 'http://www.trialmyapp.ca/#/signin';
                message = '<p> Hi</p>';
                message += '<p>This is a reminder from Trial My App. We\'re ready for you to complete the next series of questions. It should take about 10 minutes for you to complete the 1 month questionnaire or 20 minutes for you to complete the 6 month questionnaire. Please be prepared to measure your blood pressure.</p>';                
                message += '<p>Please complete the questions within the next 7 days to receive your gift card. To complete your questionnaire, log in at: trialmyapp.ca';  
                message += `<a href="${loginUrl}">${loginUrl}</a></p>`;    
                message += '<p>Questions? If you have any questions about the research now or later, or if you think you have a research-related injury, please contact Cynthia at apptrial@mcmaster.ca, 905-525-9140 x22208. If you have any questions about your rights as a research participant, please call the Office of the Chair, Hamilton Integrated Research Ethics Board at 905.521.2100 x 42013.  This study has been reviewed by HIREB, project #8039.</p>';     
    
                sendEmail({
                        to: account.email,
                        subject: 'Six Month Questionnaire Reminder',
                        html: `<p></p>
                            ${message}`
                    });
            } else {
                console.log('you already got 6 month remainder email');
            }

        }
    
    }

    });
}

async function sendGif(param){
  
    http.post({
        method: 'POST',
        url: 'https://private-anon-3096047885-giftbit.apiary-mock.com/papi/v1/campaign',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJTSEEyNTYifQ==.NlZBcXhna01KNlFOWGlrZ3hDUXJ2RmlqSm8yRzBKdHhSWnJCMUVZdXRETmZFaWlwRnRaYjlGeVdsZng4Z211Z2w1OEorVlFwRmRtT2tobjUzZmFKWklzTW03aG11eUtMSEdPY1BqVUhCSTZwWFFSZjhpcThwSGRqWkw0dUg1cWk=.8XlRptkeRSsUitGaHGxvMEokGr6zfmFddyChCRDFuoQ=',
            'Accept-Encoding': 'identity'
          },
          body:{
            "gift_template": "CJHLMKCGSRLB",
            "contacts": [
                {
                    "firstname":param.fname,
                     "lastname":param.lname,
                     "email":param.email
                 }
            ]
        }
        
        
        
        }, function (error, response, body) {
        console.log('Status:', response.statusCode);
        console.log('Headers:', JSON.stringify(response.headers));
        console.log('Response:', body);
      });
}