const { number } = require('joi');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    email: { type: String, unique: true, required: true },
    passwordHash: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    role: { type: String, required: true },
    patient_id: { type: String  },
    eligibility: { type: Number },  // 0 ineligible       1// Eligible
    profile: { type: Number },
    screeningQuestions: { type: Number,default:0 },
    zeroMonth: { type: Number,default:0 },
    zeroMonthFinishedDate: { type: String },
    oneMonth: { type: Number ,default:0},
    oneMonthFinishedDate: { type: String },
    threeMonth: { type: Number,default:0 },
    threeMonthFinishedDate: { type: String },
    sixMonth: { type: Number,default:0 },
    sixMonthFinishedDate: { type: String },
    leaveStudy: { type: Number,default:0 },
    loginTimeStamp: { type: String },
    treatment : { type: String },
    randomization_code : { type: String }, 
    zeroMonthGift:{ type: Number,default:0 },
    oneMonthGift:{ type: Number,default:0 },
    threeMonthGift:{ type: Number,default:0 },
    sixMonthGift:{ type: Number,default:0 },
    verificationToken: String,
    verified: Date,
    resetToken: {
        token: String,
        expires: Date
    },
    passwordReset: Date,
    created: { type: Date, default: Date.now },
    updated: Date
    
});

schema.virtual('isVerified').get(function () {
    return !!(this.verified || this.passwordReset);
});

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        // remove these props when object is serialized
        delete ret._id;
        delete ret.passwordHash;
    }
});

module.exports = mongoose.model('Account', schema);